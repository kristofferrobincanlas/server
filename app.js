const express = require('express');
const app = express();

const hostname = 'localhost';
const port = 8001;

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/products', (req, res) => {
  console.log('get product')
  const products = [
    { publid_id: 1, name: 'coke', price: 9 },
    { publid_id: 2, name: 'chips', price: 5 }
  ];
  res.send(products);
});

const server = app.listen(port, hostname, () => {
  const host = server.address().address
  const port = server.address().port
  console.log(`Server running at http://${host}:${port}/`);
});

